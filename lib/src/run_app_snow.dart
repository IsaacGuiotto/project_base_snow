import 'package:catcher/catcher.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'config/flavor_config.dart';
import 'models/error_reporter_handler.dart';
import 'models/flavor_values.dart';

class RunAppSnow {
  final Widget rootWidget;
  final FlavorValues flavorValues;
  final Flavor flavor;
  final GlobalKey<NavigatorState>? navigatorKey;
  final CatcherOptions? catcherDebugConfig;
  final CatcherOptions? catcherProfileConfig;
  final CatcherOptions? catcherReleaseConfig;
  final bool enableDevicePreview;
  final VoidCallback? getItInit;
  final Future<void> Function(FlutterErrorDetails errorDetails)? errorReporter;

  RunAppSnow(
    this.rootWidget, {
    this.errorReporter,
    this.getItInit,
    required this.flavorValues,
    this.flavor = Flavor.production,
    this.navigatorKey,
    this.catcherDebugConfig,
    this.catcherProfileConfig,
    this.catcherReleaseConfig,
    this.enableDevicePreview = false,
  }) {
    FlavorConfig(
      flavor: flavor,
      color: flavor == Flavor.dev ? Colors.green : Colors.deepPurpleAccent,
      values: flavorValues,
    );

    if (getItInit != null) {
      getItInit!();
    }

    final _debugOptions = CatcherOptions(SilentReportMode(), [
      ConsoleHandler(),
    ]);

    final _releaseOptions = CatcherOptions(DialogReportMode(), [
      EmailManualHandler(['email@example.com']),
    ]);

    if (errorReporter != null) {
      _debugOptions.handlers.add(ErrorReporterHandler(errorReporter!));
      _releaseOptions.handlers.add(ErrorReporterHandler(errorReporter!));
      if (catcherDebugConfig != null) {
        catcherDebugConfig?.handlers.add(ErrorReporterHandler(errorReporter!));
      }
      if (catcherReleaseConfig != null) {
        catcherReleaseConfig?.handlers
            .add(ErrorReporterHandler(errorReporter!));
      }
    }

    Catcher(
      rootWidget: rootWidget,
      navigatorKey: navigatorKey,
      debugConfig: catcherDebugConfig ?? _debugOptions,
      releaseConfig: catcherReleaseConfig ?? _releaseOptions,
      profileConfig: catcherProfileConfig,
    );
  }
}
