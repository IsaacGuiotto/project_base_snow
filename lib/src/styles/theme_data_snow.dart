import 'package:flutter/material.dart';

class ThemeDataSnow {
  static late bool _isDark;

  static void setIsDark(BuildContext context) {
    _isDark =
        MediaQuery.maybeOf(context)?.platformBrightness == Brightness.dark;
  }

  static bool get isDark => _isDark;
}
