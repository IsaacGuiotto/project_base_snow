import 'package:flutter/material.dart';

class UIHelper {
  static const _verticalSpaceExtraSmall = 5.0;
  static const _verticalSpaceSmall = 10.0;
  static const _verticalSpaceMedium = 20.0;
  static const _verticalSpaceLarge = 40.0;

  static const double _horizontalExtraSpaceSmall = 5.0;
  static const double _horizontalSpaceSmall = 10.0;
  static const double _horizontalSpaceMedium = 20.0;
  static const double _horizontalSpaceLarge = 40.0;

  static const Widget verticalSpaceExtraSmall =
      SizedBox(height: _verticalSpaceExtraSmall);
  static const Widget verticalSpaceSmall =
      SizedBox(height: _verticalSpaceSmall);
  static const Widget verticalSpaceMedium =
      SizedBox(height: _verticalSpaceMedium);
  static const Widget verticalSpaceLarge =
      SizedBox(height: _verticalSpaceLarge);

  static const Widget horizontalSpaceExtraSmall =
      SizedBox(width: _horizontalExtraSpaceSmall);
  static const Widget horizontalSpaceSmall =
      SizedBox(width: _horizontalSpaceSmall);
  static const Widget horizontalSpaceMedium =
      SizedBox(width: _horizontalSpaceMedium);
  static const Widget horizontalSpaceLarge =
      SizedBox(width: _horizontalSpaceLarge);
}
