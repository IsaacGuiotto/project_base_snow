import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_snow_base/flutter_snow_base.dart';

import 'widgets/flavor_banner/flavor_banner_widget.dart';

class SnowMaterialApp extends StatelessWidget {
  final GlobalKey<NavigatorState>? navigatorKey;
  final Widget? home;
  final Map<String, WidgetBuilder> routes;
  final String? initialRoute;
  final RouteFactory? onGenerateRoute;
  final InitialRouteListFactory? onGenerateInitialRoutes;
  final RouteFactory? onUnknownRoute;
  final List<NavigatorObserver> navigatorObservers;
  final TransitionBuilder? builder;
  final String title;
  final GenerateAppTitle? onGenerateTitle;
  final ThemeData? theme;
  final ThemeData? darkTheme;
  final ThemeMode? themeMode;
  final Color? color;
  final Locale? locale;
  final Iterable<LocalizationsDelegate<dynamic>>? localizationsDelegates;
  final LocaleListResolutionCallback? localeListResolutionCallback;
  final LocaleResolutionCallback? localeResolutionCallback;
  final Iterable<Locale> supportedLocales;
  final bool showPerformanceOverlay;
  final bool checkerboardRasterCacheImages;
  final bool checkerboardOffscreenLayers;
  final bool showSemanticsDebugger;
  final bool debugShowCheckedModeBanner;
  final Map<LogicalKeySet, Intent>? shortcuts;
  final bool debugShowMaterialGrid;
  final bool showFlavorBanner;
  final GlobalKey<ScaffoldMessengerState>? scaffoldMessengerKey;

  const SnowMaterialApp({
    Key? key,
    this.navigatorKey,
    this.scaffoldMessengerKey,
    this.home,
    this.routes = const <String, WidgetBuilder>{},
    this.initialRoute,
    this.onGenerateRoute,
    this.onGenerateInitialRoutes,
    this.onUnknownRoute,
    this.navigatorObservers = const <NavigatorObserver>[],
    this.builder,
    this.title = 'Flutter project sample base',
    this.onGenerateTitle,
    this.color,
    this.theme,
    this.darkTheme,
    this.themeMode,
    this.locale,
    this.localizationsDelegates,
    this.localeListResolutionCallback,
    this.localeResolutionCallback,
    this.supportedLocales = const [
      Locale('en', 'US'),
      Locale('pt', 'BR'),
    ],
    this.debugShowMaterialGrid = false,
    this.showPerformanceOverlay = false,
    this.checkerboardRasterCacheImages = false,
    this.checkerboardOffscreenLayers = false,
    this.showSemanticsDebugger = false,
    this.debugShowCheckedModeBanner = true,
    this.shortcuts,
    this.showFlavorBanner = true,
  }) : super(key: key);

  @override
  MaterialApp build(BuildContext context) {
    final mediaQuery = MediaQuery.maybeOf(context);

    return MaterialApp(
      title: title,
      scaffoldMessengerKey: scaffoldMessengerKey,
      locale: locale,
      builder: (context, child) {
        child ??= const SizedBox.shrink();
        return _AppBuilder(
          showFlavorBanner: showFlavorBanner,
          child: builder != null ? builder!(context, child) : child,
        );
      },
      navigatorKey: navigatorKey,
      onGenerateRoute: onGenerateRoute,
      initialRoute: initialRoute ?? '/',
      supportedLocales: supportedLocales,
      localizationsDelegates: localizationsDelegates ??
          [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
      theme: theme ??
          ThemeData(
            brightness: Brightness.light,
            accentColorBrightness: Brightness.light,
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
                TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
              },
            ),
            cupertinoOverrideTheme: CupertinoThemeData(
              brightness: Brightness.light,
            ),
          ),
      darkTheme: darkTheme ??
          ThemeData(
            brightness: Brightness.dark,
            accentColorBrightness: Brightness.dark,
            textTheme: TextTheme().apply(
              bodyColor: Colors.white,
              displayColor: Colors.white,
              decorationColor: Colors.white,
            ),
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
                TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
              },
            ),
            cupertinoOverrideTheme: CupertinoThemeData(
              brightness: Brightness.dark,
            ),
          ),
      themeMode: themeMode ??
          (mediaQuery?.platformBrightness == null
              ? ThemeMode.system
              : mediaQuery?.platformBrightness == Brightness.dark
                  ? ThemeMode.dark
                  : ThemeMode.light),
      home: home,
      routes: routes,
      onGenerateInitialRoutes: onGenerateInitialRoutes,
      onUnknownRoute: onUnknownRoute,
      navigatorObservers: navigatorObservers,
      onGenerateTitle: onGenerateTitle,
      color: color,
      localeListResolutionCallback: localeListResolutionCallback,
      localeResolutionCallback: localeResolutionCallback,
      showPerformanceOverlay: showPerformanceOverlay,
      checkerboardRasterCacheImages: checkerboardRasterCacheImages,
      checkerboardOffscreenLayers: checkerboardOffscreenLayers,
      showSemanticsDebugger: showSemanticsDebugger,
      debugShowCheckedModeBanner: debugShowCheckedModeBanner,
      shortcuts: shortcuts,
      debugShowMaterialGrid: debugShowMaterialGrid,
    ).modular();
  }
}

class _AppBuilder extends StatelessWidget {
  final Widget child;
  final bool showFlavorBanner;

  const _AppBuilder({
    Key? key,
    required this.child,
    this.showFlavorBanner = true,
  }) : super(key: key);

  Widget get body => 
        flavorBanner(child
      );



  Widget flavorBanner(Widget child) =>
      showFlavorBanner ? FlavorBannerWidget(child: child) : child;

  @override
  Widget build(BuildContext context) {
    ThemeDataSnow.setIsDark(context);
    return body;
  }
}
