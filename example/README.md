This package provides multiple examples, according to the selected architecture.

- [Daily Nasa photo](https://github.com/LucazzP/daily_nasa_photo)
- [Todo clean](https://gitlab.com/snowman-labs/flutter-devs/project_sample_base)
- [Todo modular](https://gitlab.com/snowman-labs/flutter-devs/todo-app-example-modular)
- [Example clean](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean)
- [Example clean complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_clean_complete)
- [Example MVC modular](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular)
- [Example MVC modular complete](https://gitlab.com/snowman-labs/flutter-devs/snow_cli_flutter/-/tree/master/example_mvc_modular_complete)
